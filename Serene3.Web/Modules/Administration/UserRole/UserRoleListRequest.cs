﻿using Serenity.Services;

namespace Serene3.Administration
{
    public class UserRoleListRequest : ServiceRequest
    {
        public int? UserID { get; set; }
    }
}
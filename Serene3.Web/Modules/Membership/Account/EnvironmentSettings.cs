﻿
namespace Serene3
{
    public class EnvironmentSettings
    {
        public const string SectionKey = "EnvironmentSettings";

        public string SiteExternalUrl { get; set; }
    }
}